package ru.mirsaitov.chain;

public interface Chain {

    boolean handle(Message message);

}
