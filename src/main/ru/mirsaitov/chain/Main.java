package ru.mirsaitov.chain;

public class Main {

    public static void main(String[] args) {
        JavaFilter javaFilter = new JavaFilter(null);
        Chain chain = new SpamFilter(javaFilter);

        chain.handle(new SimpleMessage("my spam"));
        chain.handle(new SimpleMessage("asdasdasdadsajavaasdasdasd"));
    }

}
