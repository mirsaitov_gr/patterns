package ru.mirsaitov.chain;

public class JavaFilter implements Chain {

    private Chain next;

    private static String[] FILTER_WORDS = {
            "Java"
    };

    public JavaFilter(Chain next) {
        this.next = next;
    }

    @Override
    public boolean handle(Message message) {
        if (message.getText() == null) {
            return true;
        }
        String text = message.getText().toLowerCase();
        for (String block : FILTER_WORDS) {
            if (text.contains(text.toLowerCase())) {
                System.out.println("JavaFilter: " + text);
                return true;
            }
        }
        if (next != null) {
            return next.handle(message);
        } else {
            return false;
        }
    }

}
