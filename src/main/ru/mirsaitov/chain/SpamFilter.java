package ru.mirsaitov.chain;

public class SpamFilter implements Chain {

    private Chain next;

    private static String[] FILTER_WORDS = {
            "Spam"
    };

    public SpamFilter(Chain next) {
        this.next = next;
    }

    @Override
    public boolean handle(Message message) {
        if (message.getText() == null) {
            return true;
        }
        String text = message.getText().toLowerCase();
        for (String block : FILTER_WORDS) {
            if (text.contains(block.toLowerCase())) {
                System.out.println("SpamFilter: " + text);
                return true;
            }
        }
        return next.handle(message);
    }

}
