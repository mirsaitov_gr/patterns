package ru.mirsaitov.chain;

public interface Message {

    String getText();

}
