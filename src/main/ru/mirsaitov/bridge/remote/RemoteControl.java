package ru.mirsaitov.bridge.remote;

import ru.mirsaitov.bridge.tv.TV;

public abstract class RemoteControl implements Control {

    private TV tv;

    public RemoteControl(TV tv) {
        this.tv = tv;
    }

    @Override
    public void on() {
        tv.on();
    }

    @Override
    public void off() {
        tv.off();
    }

    @Override
    public void switchChannel(int channel) {
        tv.switchChannel(channel);
    }

    @Override
    public void volumeUp() {
        tv.volumeUp();
    }

    @Override
    public void volumeDown() {
        tv.volumeDown();
    }

}
