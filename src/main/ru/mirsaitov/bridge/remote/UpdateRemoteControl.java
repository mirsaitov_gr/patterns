package ru.mirsaitov.bridge.remote;

import ru.mirsaitov.bridge.tv.TV;

public class UpdateRemoteControl extends RemoteControl {

    public UpdateRemoteControl(TV tv) {
        super(tv);
    }

    public void volumeUp(int volume) {
        for (int i = 0; i < volume; i++) {
            super.volumeUp();
        }
    }

    public void volumeDown(int volume) {
        for (int i = 0; i < volume; i++) {
            super.volumeDown();
        }
    }

}
