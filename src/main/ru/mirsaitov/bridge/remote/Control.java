package ru.mirsaitov.bridge.remote;

public interface Control {

    void on();

    void off();

    void switchChannel(int channel);

    void volumeUp();

    void volumeDown();

}
