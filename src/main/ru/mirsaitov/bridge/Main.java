package ru.mirsaitov.bridge;

import ru.mirsaitov.bridge.remote.UpdateRemoteControl;
import ru.mirsaitov.bridge.tv.Sony;
import ru.mirsaitov.bridge.tv.TV;

public class Main {

    public static void main(String[] args) {
        TV tv = new Sony();
        UpdateRemoteControl updateRemoteControl = new UpdateRemoteControl(tv);
        tv.on();
        System.out.println("Volume is: " + tv.getVolume());
        updateRemoteControl.volumeUp(10);
        System.out.println("Volume is: " + tv.getVolume());
    }
}
