package ru.mirsaitov.bridge.tv;

public interface TV {

    void on();

    void off();

    void switchChannel(int channel);

    void volumeUp();

    void volumeDown();

    int getVolume();

}
