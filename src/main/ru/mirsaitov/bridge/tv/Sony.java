package ru.mirsaitov.bridge.tv;

public class Sony implements TV {

    private boolean isOn = false;

    private int channel = 1;

    private int volume = 0;

    @Override
    public void on() {
        isOn = true;
    }

    @Override
    public void off() {
        isOn = false;
    }

    @Override
    public void switchChannel(int channel) {
        this.channel = channel;
    }

    @Override
    public void volumeUp() {
        if (volume < 100) {
            ++volume;
        }
    }

    @Override
    public void volumeDown() {
        if (volume > 0) {
            --volume;
        }
    }

    @Override
    public int getVolume() {
        return volume;
    }

}
