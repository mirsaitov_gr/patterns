package ru.mirsaitov.adapter;

public class Main {

    public static void main(String[] args) {
        Control controlTv = new TVControl();
        testContol("TV", controlTv);

        RemoteControl remoteControl = new RemoteControl();
        Control control = new AdapterRemoteControl(remoteControl);
        testContol("Remote", control);
    }

    private static void testContol(String name, Control control) {
        control.switchChannel(10);
        control.setSound(100);
        System.out.println(name + " channel:" + control.getChannel());
        System.out.println(name + " volume:" + control.getVolume());
    }

}
