package ru.mirsaitov.adapter;

public class TVControl implements Control {

    private int channel;

    private int volume;

    public TVControl() {
        this.channel = 1;
        this.volume = 1;
    }


    @Override
    public void switchChannel(int channel) {
        this.channel = channel;
    }

    @Override
    public int getChannel() {
        return channel;
    }

    @Override
    public void setSound(int volume) {
        this.volume = volume;
    }

    @Override
    public int getVolume() {
        return volume;
    }

}
