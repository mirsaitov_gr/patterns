package ru.mirsaitov.adapter;

public interface Control {

    void switchChannel(int channel);

    int getChannel();

    void setSound(int volume);

    int getVolume();

}
