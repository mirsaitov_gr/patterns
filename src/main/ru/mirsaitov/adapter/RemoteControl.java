package ru.mirsaitov.adapter;

public class RemoteControl {

    private int channel;

    private int volume;

    public RemoteControl() {
        this.channel = 1;
        this.volume = 1;
    }

    public int getChannel() {
        return channel;
    }

    public void setChannel(int channel) {
        this.channel = channel;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

}
