package ru.mirsaitov.adapter;

public class AdapterRemoteControl implements Control {

    private final RemoteControl remoteControl;

    public AdapterRemoteControl(RemoteControl remoteControl) {
        this.remoteControl = remoteControl;
    }

    @Override
    public void switchChannel(int channel) {
        remoteControl.setChannel(channel);
    }

    @Override
    public int getChannel() {
        return remoteControl.getChannel();
    }

    @Override
    public void setSound(int volume) {
        remoteControl.setVolume(volume);
    }

    @Override
    public int getVolume() {
        return remoteControl.getVolume();
    }

}
