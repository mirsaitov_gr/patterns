package ru.mirsaitov.command.gadjet;

public class LightBulb {

    private boolean on;

    public LightBulb() {
        on = false;
    }

    public void turnOn() {
        on = true;
    }

    public void turnOff() {
        on = false;
    }

    public boolean isOn() {
        return on;
    }

}
