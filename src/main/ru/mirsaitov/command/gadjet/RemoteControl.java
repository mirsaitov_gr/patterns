package ru.mirsaitov.command.gadjet;

import ru.mirsaitov.command.command.Command;

public class RemoteControl {

    private static int MAX = 1;

    private Command[] commandsOn = new Command[MAX];
    private Command[] commandsOff = new Command[MAX];

    public RemoteControl(Command on, Command off) {
        commandsOn[0] = on;
        commandsOff[0] = off;
    }

    public void on(int index) {
        if (index > MAX) {
            throw  new IllegalArgumentException();
        }
        commandsOn[index].execute();
    }

    public void off(int index) {
        if (index > MAX) {
            throw  new IllegalArgumentException();
        }
        commandsOff[index].execute();
    }

}
