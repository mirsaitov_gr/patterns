package ru.mirsaitov.command.gadjet;

public class TV {

    private static int MAX = 10;

    private int channel;

    private boolean on;

    public TV() {
        channel = -1;
        on = false;
    }

    public int getChannel() {
        return channel;
    }

    public boolean isOn() {
        return on;
    }

    public void on() {
        on = true;
        channel = 1;
    }

    public void off() {
        on = false;
        channel = -1;
    }

    public void upChannel() {
        if (on) {
            channel = (++channel) % MAX;
        }
    }

    public void downChannel() {
        if (on) {
            channel = --channel <= 0 ? channel : MAX;
        }
    }

}
