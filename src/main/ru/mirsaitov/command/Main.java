package ru.mirsaitov.command;

import ru.mirsaitov.command.command.*;
import ru.mirsaitov.command.gadjet.LightBulb;
import ru.mirsaitov.command.gadjet.RemoteControl;
import ru.mirsaitov.command.gadjet.TV;

public class Main {

    public static void main(String[] args) {
        LightBulb lightBulb = new LightBulb();
        TV tv = new TV();
        System.out.println("Light bulb: " + lightBulb.isOn());
        System.out.println("TV: " + tv.isOn());

        Command on = new LightOn(lightBulb);
        Command off = new LightOff(lightBulb);
        RemoteControl remoteControl = new RemoteControl(on, off);
        remoteControl.on(0);
        System.out.println("Light bulb: " + lightBulb.isOn());
        remoteControl.off(0);
        System.out.println("Light bulb: " + lightBulb.isOn());

        MacroCommand macroCommand = new MacroCommand(new Command[]{
                new LightOn(lightBulb),
                new TVOn(tv),
                new TVUpChannel(tv)
        });
        macroCommand.execute();
        System.out.println("Light bulb: " + lightBulb.isOn());
        System.out.println("TV: " + tv.isOn());
        System.out.println("TV channel: " + tv.getChannel());
    }

}
