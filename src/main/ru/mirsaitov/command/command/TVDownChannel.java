package ru.mirsaitov.command.command;

import ru.mirsaitov.command.gadjet.TV;

public class TVDownChannel implements Command {

    private final TV tv;

    public TVDownChannel(TV tv) {
        this.tv = tv;
    }

    @Override
    public void execute() {
        tv.downChannel();
    }

}
