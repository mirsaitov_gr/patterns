package ru.mirsaitov.command.command;

import ru.mirsaitov.command.gadjet.LightBulb;

public class LightOff implements Command {

    private LightBulb lightBulb;

    public LightOff(LightBulb lightBulb) {
        this.lightBulb = lightBulb;
    }

    @Override
    public void execute() {
        lightBulb.turnOff();
    }

}
