package ru.mirsaitov.command.command;

import ru.mirsaitov.command.gadjet.LightBulb;

public class LightOn implements Command {

    private LightBulb lightBulb;

    public LightOn(LightBulb lightBulb) {
        this.lightBulb = lightBulb;
    }

    @Override
    public void execute() {
        lightBulb.turnOn();
    }

}
