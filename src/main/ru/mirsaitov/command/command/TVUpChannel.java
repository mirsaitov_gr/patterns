package ru.mirsaitov.command.command;

import ru.mirsaitov.command.gadjet.TV;

public class TVUpChannel implements Command {

    private final TV tv;

    public TVUpChannel(TV tv) {
        this.tv = tv;
    }

    @Override
    public void execute() {
        tv.upChannel();
    }

}
