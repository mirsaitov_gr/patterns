package ru.mirsaitov.command.command;

import ru.mirsaitov.command.gadjet.TV;

public class TVOff implements Command {

    public TVOff(TV tv) {
        this.tv = tv;
    }

    private final TV tv;

    @Override
    public void execute() {
        tv.off();
    }

}
