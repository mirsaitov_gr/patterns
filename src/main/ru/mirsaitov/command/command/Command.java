package ru.mirsaitov.command.command;

public interface Command {

    void execute();

}
