package ru.mirsaitov.command.command;

import ru.mirsaitov.command.gadjet.TV;

public class TVOn implements Command {

    private final TV tv;

    public TVOn(TV tv) {
        this.tv = tv;
    }

    @Override
    public void execute() {
        tv.on();
    }

}
