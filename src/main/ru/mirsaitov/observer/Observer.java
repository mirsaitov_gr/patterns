package ru.mirsaitov.observer;

public interface Observer {

    void update(int i);

}
