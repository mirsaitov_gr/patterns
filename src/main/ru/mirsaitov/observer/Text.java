package ru.mirsaitov.observer;

public class Text implements Observer {

    @Override
    public void update(int i) {
        System.out.println("Text: " + i);
    }

}
