package ru.mirsaitov.observer;

public class Display implements Observer{

    @Override
    public void update(int i) {
        System.out.println("Display: " + i);
    }

}
