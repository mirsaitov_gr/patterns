package ru.mirsaitov.observer;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class Station implements Observable {

    private final Set<Observer> observers = new HashSet<>();

    private final Random random = new Random(System.currentTimeMillis());

    @Override
    public void subscribe(Observer observer) {
        if (observer == null || observers.contains(observer)) {
            return;
        }
        observers.add(observer);
    }

    @Override
    public void unsubscribe(Observer observer) {
        if (observer == null) {
            return;
        }
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        int data = random.nextInt();
        for (Observer observer : observers) {
            observer.update(data);
        }
    }
}
