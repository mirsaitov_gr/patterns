package ru.mirsaitov.observer;

public class Main {

    public static void main(String[] main) {
        Station station = new Station();
        Display display = new Display();
        Text text = new Text();
        station.subscribe(display);
        station.subscribe(text);
        station.notifyObservers();
        station.unsubscribe(display);
        station.notifyObservers();
    }

}