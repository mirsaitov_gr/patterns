package ru.mirsaitov.strategy.character;

import ru.mirsaitov.strategy.weapon.Weapon;

abstract class AbstractCharacter implements Character {

    private final String name;

    private Weapon weapon;

    public AbstractCharacter(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    @Override
    public void action() {
        weapon.action();
    }

}
