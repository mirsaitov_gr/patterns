package ru.mirsaitov.strategy.character;

import ru.mirsaitov.strategy.weapon.Weapon;

public interface Character {

    String getName();

    void setWeapon(Weapon weapon);

    void action();
}
