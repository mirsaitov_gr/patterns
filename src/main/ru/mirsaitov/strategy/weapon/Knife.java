package ru.mirsaitov.strategy.weapon;

public class Knife extends AbstractWeapon {

    public Knife() {
        super("Knife");
    }

    @Override
    public void action() {
        System.out.println("Knife attack!!!");
    }

}
