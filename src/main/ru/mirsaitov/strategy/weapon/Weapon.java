package ru.mirsaitov.strategy.weapon;

public interface Weapon {

    String getName();

    void action();

}
