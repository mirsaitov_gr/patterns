package ru.mirsaitov.strategy.weapon;

public class Sword extends AbstractWeapon {

    public Sword() {
        super("Sword");
    }

    @Override
    public void action() {
        System.out.println("Sword attack!!!");
    }

}
