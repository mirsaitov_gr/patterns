package ru.mirsaitov.strategy.weapon;

abstract class AbstractWeapon implements Weapon {

    private final String name;

    public AbstractWeapon(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public abstract void action();

}
