package ru.mirsaitov.strategy;

import ru.mirsaitov.strategy.character.Character;
import ru.mirsaitov.strategy.character.Knight;
import ru.mirsaitov.strategy.weapon.Knife;
import ru.mirsaitov.strategy.weapon.Sword;

public class Main {

    public static void main(String[] args) {
        Character character = new Knight();
        character.setWeapon(new Knife());
        character.action();
        character.setWeapon(new Sword());
        character.action();
    }

}
