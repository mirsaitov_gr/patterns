package ru.mirsaitov.facade;

public class Main {

    public static void main(String args[]) {
        TV tv = new TV();
        Light light = new Light();
        MusicCenter musicCenter = new MusicCenter();
        Control control = new Control(tv, light, musicCenter);
        control.on();
        System.out.println("TV channel and volume: " + tv.getChannel() + " " + tv.getVolume());
        System.out.println("Light is " + light.isOn());
        System.out.println("Music is " + musicCenter.getMelody());
    }

}
