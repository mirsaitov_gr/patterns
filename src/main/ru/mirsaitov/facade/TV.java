package ru.mirsaitov.facade;

public class TV {

    private int channel;

    private int volume;

    public TV() {
        this.channel = 1;
        this.volume = 1;
    }

    public int getChannel() {
        return channel;
    }

    public void setChannel(int channel) {
        this.channel = channel;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

}
