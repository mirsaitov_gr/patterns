package ru.mirsaitov.facade;

public class Control {

    private final TV tv;

    private final Light light;

    private final MusicCenter musicCenter;

    public Control(TV tv, Light light, MusicCenter musicCenter) {
        this.tv = tv;
        this.light = light;
        this.musicCenter = musicCenter;
    }

    public void on() {
        tv.setChannel(1);
        tv.setVolume(20);
        light.setOn(true);
        musicCenter.setMelody("Turn back");
    }

}
