package ru.mirsaitov.facade;

public class Light {

    private boolean on;

    public Light() {
        this.on = false;
    }

    public boolean isOn() {
        return on;
    }

    public void setOn(boolean on) {
        this.on = on;
    }

}
