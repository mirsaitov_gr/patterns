package ru.mirsaitov.facade;

public class MusicCenter {

    private String melody;

    public String getMelody() {
        return melody;
    }

    public void setMelody(String melody) {
        this.melody = melody;
    }

}
