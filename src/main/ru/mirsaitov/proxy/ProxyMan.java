package ru.mirsaitov.proxy;

public class ProxyMan implements Printable {

    private final Man man;

    public ProxyMan(Man man) {
        this.man = man;
    }

    @Override
    public void print() {
        System.out.print("My name is ");
        man.print();
    }

}
