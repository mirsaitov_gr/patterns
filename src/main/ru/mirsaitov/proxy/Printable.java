package ru.mirsaitov.proxy;

public interface Printable {

    void print();

}
