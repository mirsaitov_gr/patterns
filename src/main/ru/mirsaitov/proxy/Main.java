package ru.mirsaitov.proxy;

public class Main {

    public static void main(String[] args) {
        Man tom = new Man("Tom");
        print(tom);
        print(new ProxyMan(tom));
    }

    private static void print(Printable printable) {
        printable.print();
    }

}
