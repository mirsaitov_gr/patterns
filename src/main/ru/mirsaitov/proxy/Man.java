package ru.mirsaitov.proxy;

public class Man implements Printable {

    private final String name;

    public Man(String name) {
        this.name = name;
    }

    @Override
    public void print() {
        System.out.println(name);
    }

}
