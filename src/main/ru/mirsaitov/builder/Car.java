package ru.mirsaitov.builder;

public class Car {

    private String engine;

    private String mark;

    private String year;

    public Car(String engine, String mark, String year) {
        this.engine = engine;
        this.mark = mark;
        this.year = year;
    }

    public String getEngine() {
        return engine;
    }

    public String getMark() {
        return mark;
    }
    public String getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "Mark: " + mark + " Engine: " + engine + " Year: " + year;
    }

}
