package ru.mirsaitov.builder;

public class Main {

    public static void main(String[] args) {
        Builder builder = new CarBuilder();
        builder.setEngine("V8");
        builder.setMark("Volvo");
        builder.setYear("2011");
        System.out.println(builder.build());
        builder.setEngine("V4");
        builder.setYear("2020");
        System.out.println(builder.build());
    }

}
