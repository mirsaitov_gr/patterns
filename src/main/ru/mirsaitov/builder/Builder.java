package ru.mirsaitov.builder;

public interface Builder {

    Car build();

    void setEngine(String engine);

    void setYear(String year);

    void setMark(String mark);

}
