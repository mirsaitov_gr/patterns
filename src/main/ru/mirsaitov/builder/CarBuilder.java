package ru.mirsaitov.builder;

public class CarBuilder implements Builder {

    private String engine;

    private String mark;

    private String year;

    @Override
    public Car build() {
        return new Car(engine, mark, year);
    }

    @Override
    public void setEngine(String engine) {
        this.engine = engine;
    }

    @Override
    public void setYear(String year) {
        this.year = year;
    }

    @Override
    public void setMark(String mark) {
        this.mark = mark;
    }

}
