package ru.mirsaitov.state;

public interface State {

    void putMoney();

    void cancel();

    void sold();

    void fill(int count);

}
