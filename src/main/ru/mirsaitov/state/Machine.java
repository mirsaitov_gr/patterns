package ru.mirsaitov.state;

public interface Machine extends State {

    void setState(State state);

    int getCount();

    void addGum(int count);

    void releaseGum();

    State getFillState();

    State getPrepareState();

    State getSoldOutState();

}
