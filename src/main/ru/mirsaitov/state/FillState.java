package ru.mirsaitov.state;

public class FillState implements State {

    private final Machine machine;

    public FillState(Machine machine) {
        this.machine = machine;
    }

    @Override
    public void putMoney() {
        System.out.println("You put money!");
        machine.setState(machine.getPrepareState());
    }

    @Override
    public void cancel() {
        System.out.println("You can't insert money!");
    }

    @Override
    public void sold() {
        System.out.println("Get money!");
    }

    @Override
    public void fill(int count) {
        machine.addGum(count);
    }

}
