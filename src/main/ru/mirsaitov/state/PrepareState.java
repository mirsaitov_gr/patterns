package ru.mirsaitov.state;

public class PrepareState implements State {

    private final Machine machine;

    public PrepareState(Machine machine) {
        this.machine = machine;
    }

    @Override
    public void putMoney() {
        System.out.println("You already insert money!");
    }

    @Override
    public void cancel() {
        System.out.println("You get back money!");
        if (machine.getCount() > 0) {
            machine.setState(machine.getFillState());
        } else {
            machine.setState(machine.getSoldOutState());
        }
    }

    @Override
    public void sold() {
        System.out.println("You get gum!");
        machine.releaseGum();
        if (machine.getCount() > 0) {
            machine.setState(machine.getFillState());
        } else {
            machine.setState(machine.getSoldOutState());
        }
    }

    @Override
    public void fill(int count) {
        System.out.println("Please buy or cancel operation");
    }

}
