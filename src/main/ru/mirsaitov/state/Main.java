package ru.mirsaitov.state;

public class Main {

    public static void main(String[] args) {
        Machine machine = new GumMachine(2);

        for (int i = 0; i < 2; ++i) {
            machine.putMoney();
            machine.sold();
        }
        machine.putMoney();
        machine.fill(1);
        machine.putMoney();
        machine.cancel();
        machine.putMoney();
        machine.sold();
    }

}
