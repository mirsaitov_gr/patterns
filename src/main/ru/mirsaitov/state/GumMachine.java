package ru.mirsaitov.state;

public class GumMachine implements  Machine {

    private final FillState fillState = new FillState(this);

    private final PrepareState prepareState = new PrepareState(this);

    private final SoldOutState soldOutState = new SoldOutState(this);

    private State state;

    private int count;

    public GumMachine(int count) {
        if (count <= 0) {
            this.count = 0;
            state = new SoldOutState(this);
        } else {
            this.count = count;
            state = new FillState(this);
        }
    }

    @Override
    public void setState(State state) {
        this.state = state;
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public void addGum(int count) {
        this.count += count;
    }

    @Override
    public void releaseGum() {
        --this.count;
    }

    @Override
    public void putMoney() {
        state.putMoney();
    }

    @Override
    public void cancel() {
        state.cancel();
    }

    @Override
    public void sold() {
        state.sold();
    }

    @Override
    public void fill(int count) {
        state.fill(count);
    }

    @Override
    public State getFillState() {
        return fillState;
    }

    @Override
    public State getPrepareState() {
        return prepareState;
    }

    @Override
    public State getSoldOutState() {
        return soldOutState;
    }

}
