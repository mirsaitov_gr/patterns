package ru.mirsaitov.state;

public class SoldOutState implements State {

    private final  Machine machine;

    public SoldOutState(Machine machine) {
        this.machine = machine;
    }

    @Override
    public void putMoney() {
        System.out.println("Sorry machine sold gum");
    }

    @Override
    public void cancel() {
        System.out.println("Sorry machine sold gum");
    }

    @Override
    public void sold() {
        System.out.println("Sorry machine sold gum");
    }

    @Override
    public void fill(int count) {
        machine.addGum(count);
        machine.setState(machine.getFillState());
    }

}
