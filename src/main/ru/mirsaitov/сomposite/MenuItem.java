package ru.mirsaitov.сomposite;

public class MenuItem extends Item {

    private final String name;

    private final double cost;

    public MenuItem(String name, double cost) {
        this.name = name;
        this.cost = cost;
    }

    @Override
    public void print() {
        System.out.println(name + ": " + cost);
    }

}
