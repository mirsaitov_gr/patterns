package ru.mirsaitov.сomposite;

import java.util.Set;

public abstract class Item implements Component {

    @Override
    public void add(Component component) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void remove(Component component) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Set<Component> getChilds() {
        throw new UnsupportedOperationException();
    }

}

