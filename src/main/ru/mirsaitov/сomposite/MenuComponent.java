package ru.mirsaitov.сomposite;

import java.util.HashSet;
import java.util.Set;

public class MenuComponent implements Component {

    public MenuComponent(String name) {
        this.name = name;
    }

    private final String name;

    private final Set<Component> components = new HashSet<>();

    @Override
    public void add(Component component) {
        components.add(component);
    }

    @Override
    public void remove(Component component) {
        components.remove(component);
    }

    @Override
    public void print() {
        System.out.println(name + " menu :");
        components.forEach(item -> item.print());
    }

    @Override
    public Set<Component> getChilds() {
        return components;
    }

}
