package ru.mirsaitov.сomposite;

public class Main {

    public static void main(String[] args) {
        Component mainMenu = new MenuComponent("Main");
        mainMenu.add(new MenuItem("Beaf", 100));
        mainMenu.add(new MenuItem("Pork", 200));

        Component subMenu = new MenuComponent("Dessert");
        subMenu.add(new MenuItem("Ice cream", 50));
        subMenu.add(new MenuItem("Turkish Delight", 100));

        mainMenu.add(subMenu);
        mainMenu.print();
    }

}
