package ru.mirsaitov.сomposite;

import java.util.Set;

public interface Component {

    void add(Component component);

    void remove(Component component);

    void print();

    Set<Component> getChilds();

}
