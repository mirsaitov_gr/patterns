package ru.mirsaitov.singleton;

public class ValiotileSingleton {

    private volatile static ValiotileSingleton singleton;

    private final String url = "https://dummy.com";

    private ValiotileSingleton() {
    }

    public static ValiotileSingleton getInstance() {
        if (singleton == null) {
            synchronized (ValiotileSingleton.class) {
                if (singleton == null) {
                    singleton = new ValiotileSingleton();
                }
            }
        }
        return singleton;
    }

}
