package ru.mirsaitov.singleton;

public class SyncronizedSingleton {

    private static SyncronizedSingleton singleton;

    private final String url = "https://dummy.com";

    private SyncronizedSingleton() {
    }

    public static synchronized SyncronizedSingleton getInstance() {
        if (singleton == null) {
            singleton = new SyncronizedSingleton();
        }
        return singleton;
    }

}
