package ru.mirsaitov.singleton;

public class StaticSingleton {

    private static StaticSingleton singleton = new StaticSingleton();

    private final String url = "https://dummy.com";

    private StaticSingleton() {
    }

    public static StaticSingleton getInstance() {
        return singleton;
    }

}
