package ru.mirsaitov.singleton;

public class Main {

    public static void main(String[] args) {
        StaticSingleton ss1 = StaticSingleton.getInstance();
        StaticSingleton ss2 = StaticSingleton.getInstance();
        System.out.println("Static singleton: " + (ss1 == ss2));

        SyncronizedSingleton sync1 = SyncronizedSingleton.getInstance();
        SyncronizedSingleton sync2 = SyncronizedSingleton.getInstance();
        System.out.println("Syncronized singleton: " + (sync1 == sync2));

        ValiotileSingleton vs1 = ValiotileSingleton.getInstance();
        ValiotileSingleton vs2 = ValiotileSingleton.getInstance();
        System.out.println("Valiotile singleton: " + (vs1 == vs2));
    }

}
