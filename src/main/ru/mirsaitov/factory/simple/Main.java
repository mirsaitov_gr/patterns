package ru.mirsaitov.factory.simple;

public class Main {

    public static void main(String[] args) {
        Pizza pizza = Pizza.of(Pizza.CHEES);
        System.out.println(pizza.getName());
        pizza = Pizza.of(Pizza.VEGETERIAN);
        System.out.println(pizza.getName());
    }

}
