package ru.mirsaitov.factory.simple;

public abstract class Pizza {

    public static String CHEES = "Chees";

    public static String VEGETERIAN = "Vegeterian";

    protected String name;

    public Pizza(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static Pizza of(String name) {
        if (Pizza.CHEES.equals(name)) {
            return new Chees();
        } else if (Pizza.VEGETERIAN.equals(name)) {
            return new Vegeterian();
        } else {
            return null;
        }
    }

}
