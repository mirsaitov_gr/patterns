package ru.mirsaitov.factory.abstractfactory;

public abstract class Chees {

    private String name;

    public Chees(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
