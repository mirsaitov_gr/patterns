package ru.mirsaitov.factory.abstractfactory;

public abstract class Tomato {

    private String name;

    public Tomato(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
