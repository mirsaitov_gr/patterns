package ru.mirsaitov.factory.abstractfactory.boston;

import ru.mirsaitov.factory.abstractfactory.Chees;

public class BostonChees extends Chees {

    public BostonChees() {
        super("BostonChees");
    }

}
