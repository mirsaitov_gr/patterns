package ru.mirsaitov.factory.abstractfactory.boston;

import ru.mirsaitov.factory.abstractfactory.Tomato;

public class BostonTomato extends Tomato {

    public BostonTomato() {
        super("BostonTomato");
    }

}
