package ru.mirsaitov.factory.abstractfactory.boston;

import ru.mirsaitov.factory.abstractfactory.AbstractFactory;
import ru.mirsaitov.factory.abstractfactory.Chees;
import ru.mirsaitov.factory.abstractfactory.Tomato;

public class BostonFactory implements AbstractFactory {

    @Override
    public Chees getChees() {
        return new BostonChees();
    }

    @Override
    public Tomato getTomato() {
        return new BostonTomato();
    }

}
