package ru.mirsaitov.factory.abstractfactory;

import ru.mirsaitov.factory.abstractfactory.boston.BostonFactory;
import ru.mirsaitov.factory.abstractfactory.newyork.NewYorkFactory;

public class Main {

    public static void main(String[] args) {
        printIngredient(new BostonFactory());
        printIngredient(new NewYorkFactory());
    }

    private static void printIngredient(AbstractFactory abstractFactory) {
        System.out.println("Chees: " + abstractFactory.getChees().getName());
        System.out.println("Tomato: " + abstractFactory.getTomato().getName());
    }

}
