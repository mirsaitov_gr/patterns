package ru.mirsaitov.factory.abstractfactory;

public interface AbstractFactory {

    Chees getChees();

    Tomato getTomato();

}
