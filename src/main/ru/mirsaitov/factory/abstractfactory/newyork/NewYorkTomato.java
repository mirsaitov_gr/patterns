package ru.mirsaitov.factory.abstractfactory.newyork;

import ru.mirsaitov.factory.abstractfactory.Tomato;

public class NewYorkTomato extends Tomato {

    public NewYorkTomato() {
        super("NewYorkTomato");
    }

}
