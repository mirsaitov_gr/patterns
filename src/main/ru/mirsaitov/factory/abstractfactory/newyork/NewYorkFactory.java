package ru.mirsaitov.factory.abstractfactory.newyork;

import ru.mirsaitov.factory.abstractfactory.AbstractFactory;
import ru.mirsaitov.factory.abstractfactory.Chees;
import ru.mirsaitov.factory.abstractfactory.Tomato;

public class NewYorkFactory implements AbstractFactory {

    @Override
    public Chees getChees() {
        return new NewYorkChees();
    }

    @Override
    public Tomato getTomato() {
        return new NewYorkTomato();
    }

}
