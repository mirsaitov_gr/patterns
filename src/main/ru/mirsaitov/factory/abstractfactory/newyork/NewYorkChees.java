package ru.mirsaitov.factory.abstractfactory.newyork;

import ru.mirsaitov.factory.abstractfactory.Chees;

public class NewYorkChees extends Chees {

    public NewYorkChees() {
        super("NewYorkChees");
    }

}
