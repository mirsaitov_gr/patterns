package ru.mirsaitov.factory.factorymethod.boston;

import ru.mirsaitov.factory.factorymethod.Pizza;

public class Chees extends Pizza {

    public Chees() {
        super("Boston cheese");
    }

}
