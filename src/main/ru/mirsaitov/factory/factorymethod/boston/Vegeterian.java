package ru.mirsaitov.factory.factorymethod.boston;

import ru.mirsaitov.factory.factorymethod.Pizza;

public class Vegeterian extends Pizza {

    public Vegeterian() {
        super("Boston vegeterian");
    }

}
