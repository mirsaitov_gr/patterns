package ru.mirsaitov.factory.factorymethod;

public abstract class Pizza {

    public static String CHEES = "Chees";

    public static String VEGETERIAN = "Vegeterian";

    protected String name;

    public Pizza(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
