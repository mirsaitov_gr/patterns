package ru.mirsaitov.factory.factorymethod;

import ru.mirsaitov.factory.factorymethod.boston.BostonPizzaFactory;
import ru.mirsaitov.factory.factorymethod.newyork.NewYorkPizzaFactory;

public class Main {

    public static void main(String[] args) {
        PizzaFactory factory = new BostonPizzaFactory();
        factory.orderPizza(Pizza.CHEES);
        factory.orderPizza(Pizza.VEGETERIAN);
        factory = new NewYorkPizzaFactory();
        factory.orderPizza(Pizza.CHEES);
        factory.orderPizza(Pizza.VEGETERIAN);
    }

}

