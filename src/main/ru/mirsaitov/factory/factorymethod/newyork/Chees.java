package ru.mirsaitov.factory.factorymethod.newyork;

import ru.mirsaitov.factory.factorymethod.Pizza;

public class Chees extends Pizza {

    public Chees() {
        super("New York cheese");
    }

}
