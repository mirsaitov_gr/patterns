package ru.mirsaitov.factory.factorymethod.newyork;

import ru.mirsaitov.factory.factorymethod.Pizza;

public class Vegeterian extends Pizza {

    public Vegeterian() {
        super("New York vegeterian");
    }

}
