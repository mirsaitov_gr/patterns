package ru.mirsaitov.factory.factorymethod.newyork;

import ru.mirsaitov.factory.factorymethod.Pizza;
import ru.mirsaitov.factory.factorymethod.PizzaFactory;

public class NewYorkPizzaFactory extends PizzaFactory {

    @Override
    protected Pizza getPizza(String name) {
        if (Pizza.CHEES.equals(name)) {
            return new Chees();
        } else if (Pizza.VEGETERIAN.equals(name)) {
            return new Vegeterian();
        } else {
            return null;
        }
    }

}
