package ru.mirsaitov.factory.factorymethod;

public abstract class PizzaFactory {

    public void orderPizza(String name) {
        Pizza pizza = getPizza(name);
        coocking(pizza);
        cut();
        box();
    }

    protected abstract Pizza getPizza(String name);

    private void coocking(Pizza pizza) {
        System.out.println("Coocking pizza " + pizza.getName());
    }

    private void cut() {
        System.out.println("Cut pizza!");
    }

    private void box() {
        System.out.println("Box pizza!");
    }

}
