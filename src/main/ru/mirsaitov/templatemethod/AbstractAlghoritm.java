package ru.mirsaitov.templatemethod;

public abstract class AbstractAlghoritm {

    private final double baseCost;

    public AbstractAlghoritm(double baseCost) {
        this.baseCost = baseCost;
    }

    public final double compute() {
        return baseCost + taxCompute(baseCost);
    }

    protected abstract double taxCompute(double baseCost);

}
