package ru.mirsaitov.templatemethod;

public class Main {

    public static void main(String[] args) {
        AbstractAlghoritm russian = new RussianTax(100);
        AbstractAlghoritm usa = new USATax(100);
        System.out.println("Total in Russian Federation: " + russian.compute());
        System.out.println("Total in USA: " + usa.compute());
    }

}
