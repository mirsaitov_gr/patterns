package ru.mirsaitov.templatemethod;

public class RussianTax extends AbstractAlghoritm
{
    public RussianTax(double baseCost) {
        super(baseCost);
    }

    @Override
    public double taxCompute(double baseCost) {
        return baseCost * 0.13;
    }

}
