package ru.mirsaitov.templatemethod;

public class USATax extends AbstractAlghoritm {

    public USATax(double baseCost) {
        super(baseCost);
    }

    @Override
    public double taxCompute(double baseCost) {
        return baseCost < 1000 ? baseCost * 0.1 : baseCost * 0.2;
    }

}
