package ru.mirsaitov.iterator;

public class ArrayItareble implements Iterable<Integer> {

    private final int[] array;

    public ArrayItareble(int[] array) {
        this.array = array;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new ArrayIterator(array);
    }

}
