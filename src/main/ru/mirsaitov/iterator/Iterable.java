package ru.mirsaitov.iterator;

public interface Iterable<T> {

    Iterator<T> iterator();

}
