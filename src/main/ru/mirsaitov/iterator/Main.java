package ru.mirsaitov.iterator;

public class Main {

    public static void main(String[] args) {
        int[] array = {2, 3};

        Iterable<Integer> iterable = new ArrayItareble(array);
        Iterator<Integer> iterator = iterable.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

}
