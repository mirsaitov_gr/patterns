package ru.mirsaitov.iterator;

public class ArrayIterator implements Iterator<Integer> {

    private final int[] array;

    int current = 0;

    public ArrayIterator(int[] array) {
        this.array = array;
    }

    @Override
    public boolean hasNext() {
        return current < array.length;
    }

    @Override
    public Integer next() {
        return array[current++];
    }

    @Override
    public void remove(Integer element) {
        throw new UnsupportedOperationException();
    }

}
