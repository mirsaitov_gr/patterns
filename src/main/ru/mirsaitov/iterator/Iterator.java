package ru.mirsaitov.iterator;

public interface Iterator<T> {

    boolean hasNext();

    T next();

    void remove(T element);
    
}
