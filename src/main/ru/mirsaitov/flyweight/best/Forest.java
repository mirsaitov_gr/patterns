package ru.mirsaitov.flyweight.best;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Forest {

    private static String[] COLOR = {
            "Green",
            "Yellow"
    };

    private static String[] NAME = {
            "Oak",
            "Birch"
    };

    private int size;

    private int[][] trees;

    private TreeType[] types;

    private static List<TreeType> typeList = new ArrayList<>();

    public Forest(int size) {
        this.size = size;
        trees = new int[2][size];
        types = new TreeType[size];
        generate();
    }

    private void generate() {
        Random random = new Random(System.currentTimeMillis());
        for (int i = 0; i < size; ++i) {
            trees[0][i] = random.nextInt();
            trees[1][i] = random.nextInt();

            String rndColor = COLOR[random.nextInt(COLOR.length)];
            String rndName = NAME[random.nextInt(NAME.length)];

            TreeType treeType = typeList.stream()
                    .filter(type -> type.getName().equals(rndName) && type.getColor().equals(rndColor))
                    .findFirst()
                    .orElseGet(() -> {
                        TreeType crtTreeType = new TreeType(rndColor, rndName);
                        typeList.add(crtTreeType);
                        return crtTreeType;
                    });

            types[i] = treeType;
        }
    }

}
