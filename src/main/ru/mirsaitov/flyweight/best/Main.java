package ru.mirsaitov.flyweight.best;

public class Main {

    private static int SIZE = 100_000_000;

    public static void main(String[] args) {
        Runtime runtime = Runtime.getRuntime();
        long before = runtime.totalMemory() - runtime.freeMemory();
        Forest forest = new Forest(SIZE);
        long after = runtime.totalMemory() - runtime.freeMemory();
        System.out.println(after - before);
    }

}
