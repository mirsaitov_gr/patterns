package ru.mirsaitov.flyweight.refactor;

public class TreeType {

    public String getColor() {
        return color;
    }

    public String getName() {
        return name;
    }

    private String color;

    private String name;

    public TreeType(String color, String name) {
        this.color = color;
        this.name = name;
    }

}
