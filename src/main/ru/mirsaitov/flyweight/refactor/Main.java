package ru.mirsaitov.flyweight.refactor;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    private static String[] COLOR = {
            "Green",
            "Yellow"
    };

    private static String[] NAME = {
            "Oak",
            "Birch"
    };

    private static List<TreeType> typeList = new ArrayList<>();

    private static int SIZE = 100_000_000;

    public static void main(String[] args) {
        Runtime runtime = Runtime.getRuntime();
        long before = runtime.totalMemory() - runtime.freeMemory();
        Tree[] tree = oldTree(SIZE);
        long after = runtime.totalMemory() - runtime.freeMemory();
        System.out.println(after - before);
    }

    public static Tree[] oldTree(int size) {
        Random random = new Random(System.currentTimeMillis());
        Tree[] tree = new Tree[size];
        for (int i = 0; i < size; ++i) {
            String rndColor = COLOR[random.nextInt(COLOR.length)];
            String rndName = NAME[random.nextInt(NAME.length)];

            TreeType treeType = typeList.stream()
                .filter(type -> type.getName().equals(rndName) && type.getColor().equals(rndColor))
                    .findFirst()
                    .orElseGet(() -> {
                       TreeType crtTreeType = new TreeType(rndColor, rndName);
                       typeList.add(crtTreeType);
                       return crtTreeType;
                    });

            tree[i] = new Tree(random.nextInt(),
                    random.nextInt(),
                    treeType);
        }
        return tree;
    }

}
