package ru.mirsaitov.flyweight.old;

public class Tree {

    private int x;

    private int y;

    private String color;

    private String name;

    public Tree(int x, int y, String color, String name) {
        this.x = x;
        this.y = y;
        this.color = color;
        this.name = name;
    }
}
