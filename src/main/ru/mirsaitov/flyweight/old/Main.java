package ru.mirsaitov.flyweight.old;

import java.util.Random;

public class Main {

    private static String[] COLOR = {
            "Green",
            "Yellow"
    };

    private static String[] TYPE = {
            "Oak",
            "Birch"
    };

    private static int SIZE = 100_000_000;

    public static void main(String[] args) {
        Runtime runtime = Runtime.getRuntime();
        long before = runtime.totalMemory() - runtime.freeMemory();
        Tree[] tree = oldTree(SIZE);
        long after = runtime.totalMemory() - runtime.freeMemory();
        System.out.println(after - before);
    }

    public static Tree[] oldTree(int size) {
        Random random = new Random(System.currentTimeMillis());
        Tree[] tree = new Tree[size];
        for (int i = 0; i < size; ++i) {
            tree[i] = new Tree(random.nextInt(),
                    random.nextInt(),
                    COLOR[random.nextInt(COLOR.length)],
                    TYPE[random.nextInt(TYPE.length)]);
        }
        return tree;
    }

}
