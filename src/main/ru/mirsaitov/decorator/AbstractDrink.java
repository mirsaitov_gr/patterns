package ru.mirsaitov.decorator;

public abstract class AbstractDrink {

    protected String name;

    protected double cost;

    public AbstractDrink(String name, double cost) {
        this.name = name;
        this.cost = cost;
    }

    public double getCost() {
        return cost;
    }

    public String getName() {
        return name;
    }

}
