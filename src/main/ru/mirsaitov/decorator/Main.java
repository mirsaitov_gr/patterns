package ru.mirsaitov.decorator;

public class Main {

    public static void main(String[] args) {
        AbstractDrink coffeeWithMocha = new Mocha(new Coffee());
        System.out.println(coffeeWithMocha.getName() + " " + coffeeWithMocha.getCost());
        coffeeWithMocha = new Mocha(coffeeWithMocha);
        System.out.println(coffeeWithMocha.getName() + " " + coffeeWithMocha.getCost());
    }

}
