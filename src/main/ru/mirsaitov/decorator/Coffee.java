package ru.mirsaitov.decorator;

public class Coffee extends AbstractDrink {

    public Coffee() {
        super("Coffee", 10);
    }

}
