package ru.mirsaitov.decorator;

public abstract class Decorator extends AbstractDrink {

    protected final AbstractDrink abstractDrink;

    public Decorator(AbstractDrink abstractDrink) {
        super(abstractDrink.getName(), abstractDrink.getCost());
        this.abstractDrink = abstractDrink;
    }

}
