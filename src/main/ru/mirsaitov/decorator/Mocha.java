package ru.mirsaitov.decorator;

public class Mocha extends Decorator {

    public Mocha(AbstractDrink abstractDrink) {
        super(abstractDrink);
        this.name = "Mocha";
    }

    @Override
    public double getCost() {
        return super.getCost() + 5;
    }

    @Override
    public String getName() {
        return abstractDrink.getName() + " with " + this.name;
    }

}
